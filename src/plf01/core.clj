(defn función-<-1
[a, b]
(< a b))

(defn función-<-2
[a, b ]
(< (* 2 a) b))

(defn función-<-3
[a, b, c]
(< a b c))

(función-<-1 1 2) 
(función-<-2 3 4)
(función-<-3 4 5 6)

(defn función-<=-1
[a, b]
(<= a b))

(defn función-<=-2
[a, b ]
(<= (* 2 a) b))

(defn función-<=-3
[a, b, c]
(<= a b c))

(función-<=-1 1 2) 
(función-<=-2 3 4)
(función-<=-3 1/2 1/3 1/4)

(defn función-==-1
[a]
(== a))

(defn función-==-2
[a, b]
(== a, b)

(defn función-==-3
[a, b, c]
(== a, b, c))

(función->-1 1) 
(función->-2 3 4)
(función->-3 1/2 1/3 1/4)

(defn función->-1
[a, b]
(> a b))

(defn función->2
[a, b]
(> a, b))

(defn función->-3
[a, b, c, d, e]
(> a, b, c, d, e))

(función->-1 1 2)
(función->-2 2 1)
(función->-3 6 5 4 3 2)

(defn función->=-1
[a, b]
(>= a b))

(defn función->=-2
[a, b]
(>= a, b))

(función->=-1 2 1)
(función->=-2 2 2)
(función->=-3 6 5 4 3 2)

(defn función->=-3
[a, b, c, d, e]
(>= a, b, c, d, e))

(función->=-1 2 1)
(función->=-2 1/2 0.5)
(función->=-3 6 5 4 3 2)

(defn función-assoc-1
[a, b, c]
(assoc [a, b, c] 0 10 ))

(defn función-assoc-2
[a, b, c]
(assoc [a, b, c] 3 10 ))

(defn función-assoc-3
[a, b, c]
(asssoc [a, b, c] 4 10)

(función-assoc-1 1 2 3)
(función-assoc-2 1 2 3)
(función-assoc-2 1 2 3)

(defn función-concat-1
[a b c d]
(concat [a b] [c d] ))

(defn función-concat-2
[a b c d e]
(concat [a b] [c] [d e] ))

(defn función-concat-3
[a b c]
(concat {:a a :b b} {:c c))

(función-concat-1 1 2 3 4)
(función-concat-2 1 2 3 4 5)
(función-concat-3 1 2 3)
